import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Patryk on 2017-12-22.
 */
public class XMLParser {
    private static final String MAIN_ELEMENT = "Message";

    public static Document getDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(file);
    }

    public static Request parseXml(Document xmlFile) throws Exception {
        xmlFile.getDocumentElement().normalize();

        NodeList nodeList = xmlFile.getElementsByTagName(MAIN_ELEMENT);

        if(nodeList.getLength() != 1) {
            throw new Exception ("Wrongly built XML file");
        }

        Element mainElement = (Element) nodeList.item(0);

        Node requestIdNode = mainElement.getElementsByTagName("RequestID").item(0).getFirstChild();
        Node senderIpNode = mainElement.getElementsByTagName("SenderIP").item(0).getFirstChild();
        Node codeNode = mainElement.getElementsByTagName("Code").item(0).getFirstChild();
        Node contentNode = mainElement.getElementsByTagName("Content").item(0).getFirstChild();

        String[] senderIp = senderIpNode.getNodeValue().split(":");

        Task task = new Task(requestIdNode.getNodeValue(), new IfAddress(senderIp[0], Integer.parseInt(senderIp[1])), contentNode.getNodeValue());
        Request request = new Request(task, Integer.parseInt(codeNode.getNodeValue()));

        return request;
    }

}
