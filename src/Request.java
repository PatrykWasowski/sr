/**
 * Created by Patryk on 2017-12-22.
 */
public class Request {
    private Task task;
    private int code;

    public Request(Task task, int code) {
        this.task = task;
        this.code = code;
    }

    public Task getTask() {
        return task;
    }

    public int getCode() {
        return code;
    }
}
