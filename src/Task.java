import java.net.Socket;

/**
 * Created by Patryk on 2017-12-22.
 */
public class Task {
    private String requestId;
    private IfAddress taskSender;
    private IfAddress taskExecutor;
    private String taskCode;
    private State taskState;

    private String result;


    public enum State {
        IN_PROGRESS, FINISHED;
    }

    public Task(String requestId, IfAddress taskSender, String taskCode) {
        this.requestId = requestId;
        this.taskSender = taskSender;
        this.taskExecutor = null;
        this.taskCode = taskCode;
        taskState = State.IN_PROGRESS;
    }

    public boolean isFinished() {
        return taskState == State.FINISHED;
    }

    public String getResult() throws Exception {
        if(!isFinished()) {
            throw new Exception("Task is not finished yet");
        }
        return result;
    }

    public String getRequestId() {
        return requestId;
    }

    public IfAddress getTaskSender() {
        return taskSender;
    }

    public IfAddress getTaskExecutor() {
        return taskExecutor;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public State getTaskState() {
        return taskState;
    }
}
