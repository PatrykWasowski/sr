/**
 * Created by Patryk on 2017-12-23.
 */
public class IfAddress {
    private String ipAddress;
    private int port;

    public IfAddress(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }

    public boolean equals (IfAddress ifAddress) {
        return (getIpAddress().equals(ifAddress.getIpAddress()) && getPort() == ifAddress.getPort());
    }
}
