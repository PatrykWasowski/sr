import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.Socket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Patryk on 2017-12-22.
 */
public class XMLParserTest {
    private static final String XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<Message>\n" +
            "<SenderIP>192.168.1.1:81</SenderIP>\n" +
            "<RequestID>20171222212112</RequestID>\n" +
            "<Code>11</Code>\n" +
            "<Content>print(\"Hello World!\")</Content>\n" +
            "</Message>\n";
    private static final String testFilePath = "test_file.xml";

    private DocumentBuilderFactory documentBuilderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;

    private File testFile;

    @Before
    public void init() throws ParserConfigurationException, IOException, SAXException {
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        document = documentBuilder.parse(new InputSource(new StringReader(XML)));

        testFile = new File(testFilePath);
    }

    @Test
    public void getDocumentFromFileTest() throws IOException, SAXException, ParserConfigurationException {
        Document testDocument = XMLParser.getDocumentFromFile(testFile);

        assertEquals(document.getDocumentElement().getNodeName(), testDocument.getDocumentElement().getNodeName());
    }

    @Test
    public void parseXmlTest() throws Exception {
        Request request = XMLParser.parseXml(document);
        Task task = request.getTask();

        assertEquals("20171222212112", task.getRequestId());
        assertTrue(new IfAddress("192.168.1.1", 81).equals(task.getTaskSender()));
        assertEquals(null, task.getTaskExecutor());
        assertEquals("print(\"Hello World!\")", task.getTaskCode());
        assertEquals(11, request.getCode());
        assertEquals(Task.State.IN_PROGRESS, task.getTaskState());
    }
}